@extends('layouts.main')

@section('head')
<link href="{{asset('css/video-js.css')}}" rel="stylesheet" />
@stop

@section('content')

<div class="container">
<div class="row justify-content-center mb-0 d-lg-none">
	<div class="col">
		<div class="d-flex justify-content-center" style="font-size:36px; height:55px; overflow:hidden;">
			<div class="text-white">{{$chapter->title}}</div>
		</div>
	</div>
</div>
</div>
<div class="container">
	<div class="row justify-content-center">
		<div class="@if($orientation == 'portrait') col-lg-3 @else col-lg-7 @endif pt-3">
			<div id="main_video_poster" class="card text-white mb-3" style="background-color: black;">
				<img class="card-img" src="{{$lesson->thumb}}">
				<div class="card-img-overlay text-right d-none d-md-block">
					<div class="" style="position: absolute; bottom: 10px; right: 20px;">
						<div class="d-flex">
							<div class="flex-fill" style="font-size:36px; padding-left: 140px; margin-right: 20px; 
								position: relative; bottom: 10px; height: 50px;overflow:hidden;">
								{{$lesson->title}}
							</div>
							<div><img style="width:100px;" src="{{asset('img/play.png')}}"></div>
						</div>
					</div>
				</div>
				<div class="card-img-overlay d-md-none">
					<div class="" style="position: absolute; bottom: 5px; right: 15px;">
						<div class="d-flex" >
							<div style="font-size:24px; padding-left: 120px;margin-right: 20px; 
								position: relative; top: 5px; height: 45px;overflow:hidden;">
								{{$lesson->title}}
							</div>
							<div><img style="width:65px;" src="{{asset('img/play.png')}}"></div>
						</div>
					</div>
				</div>
			</div>
			<div id="main_video_wrapper" style="display:none;">	
				<video-js 
					id="main_video" 
					class="w-100 mb-4 video-js vjs-default-skin vjs-fluid vjs-controls-enabled vjs-workinghover vjs-v7 vjs-has-started vjs-playing vjs-user-inactive" 
					data-setup='{"fluid": true}' 
					controls 
					playsinline >
					<source src="{{$lesson->video}}" type="video/mp4" />
				</video-js>
			</div>
			<div class="d-flex justify-content-between">
				<div class="">
					<div>วิดีโอที่กำลังเรียน: {{$lesson->title}}</div>
					<div>{{$lesson->description}}</div>
					@if(!empty($chapter->doc))
						<div>
							Download: <a href="{{$chapter->doc}}">เอกสาร</a>
						</div>
					@endif
				</div>
				<div class="d-none d-sm-block text-nowrap">{!! $previous !!} @if(!empty($previous) && !empty($next)) | @endif {!! $next !!}</div>
			</div>
			<div class="d-sm-none text-nowrap">{!! $previous !!} @if(!empty($previous) && !empty($next)) | @endif  {!! $next !!}</div>
		</div>
		<div class="col-lg-5">
			<div class="row">
				<div class="col">
					<div class="row">
						<div class="col-lg-12 pt-3">		
							<div class="px-3 pb-3 rounded" style="background-color: #0F1A29; border:2px solid grey;">					
								<div class="text-center pb-2" style="color: rgb(146,146,146); border-bottom: 1px solid grey;">
									<div class="text-white pt-2" style="font-size: 36px;">
										{{$chapter->title}}
									</div>
									<div>
										เวลาทั้งหมด: {{$chapter->duration_show}}
									</div>
									<div>
										<em>{{$chapter->description}}</em>
									</div>
								</div>
								<div id="overflow-menu" style=" height:362px; overflow:auto;" class="mb-4 pt-3 pb-3">
									{!! $chapter_menu !!}
								</div>
								<div class="text-center pb-2" style="border-top: 1px solid grey;">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row justify-content-center text-center">
		<div class="d-lg-none" style="font-size: 42px;">
			เนื้อหาที่เกี่ยวข้อง
		</div>
		<div class="col pb-2 d-none display-4 d-lg-block">
			เนื้อหาที่เกี่ยวข้อง
		</div>
	</div>
	<div class="row">
		@foreach($course->chapters()->orderBy('chapter_course.position')->get() as $chapter)
			<div class="col-md-6 col-lg-4 col-xl-3 mb-2 d-none d-md-block pe-2">
	            <div class="card bg-dark text-white text-center" style="border: 2px solid grey;">
	                <img loading="lazy" class="card-img" src="{{$chapter->thumb}}?fit=400%2C400" alt="Card image">
	                <a style="color:inherit;" href="{{route('course.show',[$course->post_id,$chapter->post_id])}}">
	                    <div class="card-img-overlay h-100 d-flex flex-column justify-content-end pb-0">
	                        <h4 class="card-title">{{$chapter->title}}</h4>
	                    </div>
	                </a>
	            </div>
	        </div>
	        <div class="d-md-none w-100 mb-2">
	            <a class="" style="color:inherit;" href="{{route('course.show',[$course->post_id,$chapter->post_id])}}">
	                <div class="d-flex">
	                    <div class="col-4 me-3">
	                        <img class="rounded w-100" loading="lazy" src="{{$chapter->thumb}}?fit=200%2C200">
	                    </div>
	                    <div class="col d-flex align-items-center">
	                        {{$chapter->title}}
	                    </div>
	                </div>
	            </a>
	        </div>
		@endforeach
	</div>
</div>
@stop

@section('foot')
<script>
	$(function(){
		$('#main_video_poster').css( 'cursor', 'pointer' );
		$('body').on("click tap","#main_video_poster", function(e){
			$(this).hide();
			$('#main_video_wrapper').show();
			var player = videojs('main_video');
			player.play();
		});
		$('#main_video').bind('contextmenu',function() { return false; });
		$('#overflow-menu').scrollTop(0);
		var the_offset = $("#anchor").offset().top - $("#overflow-menu").offset().top - 10;
		if(the_offset>0){
			$('#overflow-menu').scrollTop(the_offset);
		}

	});
</script>
<script src="{{asset('js/video.js')}}"></script>
<script>
	$(function(){
		$("body").on("mouseover",".card",function(){
			$(this).css("border","solid 2px white");
		});
		$("body").on("mouseout",".card",function(){
			$(this).css("border","solid 2px grey");
		});
	});
</script>
@stop