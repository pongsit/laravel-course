<?php

namespace Pongsit\Course\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Chapter extends Model
{
  use HasFactory;

  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  public function courses(){
    return $this->belongsToMany('Pongsit\Course\Models\Course')->withPivot('position')->withTimestamps();
  }

  public function lessons(){
    return $this->belongsToMany('Pongsit\Course\Models\Lesson')->withPivot('position')->withTimestamps();
  }

}
