<?php

namespace Pongsit\Course\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Lesson extends Model
{
  use HasFactory;

  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  public function chapters() {
      return $this->belongsToMany('Pongsit\Course\Models\Chapter')->withPivot('position')->withTimestamps();
  }

}
