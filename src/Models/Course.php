<?php

namespace Pongsit\Course\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Course extends Model
{
  use HasFactory;

  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  public function chapters() {
      return $this->belongsToMany('Pongsit\Course\Models\Chapter')->withPivot('position')->withTimestamps();
  }

  public function sync($course_id){

    $course_json = file_get_contents('https://'.idn_to_ascii('คอร์ส').'.com/get_course_by_id.php?author=ajnunu&course_id='.$course_id.'&time='.rand(1000,9999));
    $course_infos = json_decode($course_json,true);
    $course_preps['post_id'] = $course_infos['id'];
    $course_preps['title'] = $course_infos['title'];
    $course_preps['description'] = $course_infos['description'];
    Course::upsert($course_preps,'post_id');
    $course = Course::where('post_id',$course_id)->first();

    $chapter_syncs = [];
    foreach($course_infos['chapters'] as $k => $chapters){
        $chapter_preps['post_id'] = $chapters['id'];
        $chapter_preps['title'] = $chapters['title'];
        $chapter_preps['thumb'] = $chapters['thumb'];
        $chapter_preps['doc'] = $chapters['doc'];
        $chapter_preps['doc2'] = $chapters['doc2'];
        $chapter_preps['doc_sample'] = $chapters['doc_sample'];
        $chapter_preps['visibility'] = $chapters['visibility'];
        Chapter::upsert($chapter_preps,'post_id');
        $chapter = Chapter::where('post_id',$chapters['id'])->first();
        $chapter_syncs[$chapter->id]['position'] = $k+1;

        $lesson_syncs = [];
        foreach($chapters['lessons'] as $_k => $lessons){
            $lesson_preps['post_id'] = $lessons['id'];
            $lesson_preps['title'] = $lessons['title'];
            $lesson_preps['shortcode'] = $lessons['shortcode'];
            Lesson::upsert($lesson_preps,'post_id');
            $lesson = Lesson::where('post_id',$lessons['id'])->first();
            $lesson_syncs[$lesson->id]['position'] = $_k+1;
        }
        $chapter->lessons()->sync($lesson_syncs);
    }
    $course->chapters()->sync($chapter_syncs);
  }

}
