<?php

namespace Pongsit\Course\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Pongsit\Course\Models\Course;
use Pongsit\Course\Models\Chapter;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FirstName  $firstName
     * @return \Illuminate\Http\Response
     */
    public function show($course_id,$chapter_id,$lesson_id='')
    {
        if(!is_numeric($course_id)){
            return redirect('/')->with(['error'=>'มีผิดพลาดบางอย่าง กรุณาลองใหม่อีกครั้ง']);
        }

        if(!user()->isLogin()){
        	return redirect('/');
        }

        $update = 0;
        if(!empty($_GET['update'])){
        	$update = 1;
        }

        $variables['course_id'] = $course_id;
        $variables['chapter_id'] = $chapter_id;

        $course = Course::where('post_id',$course_id)->first();
        if(empty($course->id)){
        	return redirect('/');
        }

        if($update == 1){
			$c = new Course;
            $c->sync($course_id);
            $course = Course::where('post_id',$course_id)->first();
        }

        $chapter = $course->chapters->where('post_id',$chapter_id)->first();

        if(empty($lesson_id)){
        	$lesson = $chapter->lessons()->orderBy('chapter_lesson.position')->first();
	        $lesson_id = $lesson->post_id;
	    }

	    $variables['lesson_id'] = $lesson_id;

		if(empty($chapter->duration) || $update == 1){
			$comma = '';
			$query_api = '';
			foreach($course->chapters->where('post_id',$chapter_id)->first()->lessons as $v){
				$query_api .= $comma.'urls[]=/videos/'.$v->shortcode;
				$comma = '&';
			}

			$video_json = file_get_contents('https://public-api.wordpress.com/rest/v1/batch/?'.$query_api);
			$video_infos = json_decode($video_json,true);

			$chapter->duration = 0;
			foreach($video_infos as $vs){
				$chapter->duration += $vs['duration'];
				$l = $chapter->lessons->where('post_id',$vs['post_id'])->first();
				$l->duration = $vs['duration'];
				$l->duration_show = $this->formatMilliseconds($vs['duration']);
				$l->video = $vs['original'];
				$l->thumb = $vs['poster'];
				$l->save();
			}

			$chapter->duration_show = $this->formatMilliseconds($chapter->duration);
			$chapter->save();
		}

        $lesson = $chapter->lessons->where('post_id',$lesson_id)->first();

        $variables['course'] = $course;
        $variables['chapter'] = $chapter;
        $variables['lesson'] = $lesson;
		$variables['previous'] = '';
		$variables['next'] = '';
		$variables['chapter_menu'] = '';

		$dimension = getimagesize($lesson->thumb);
		$width = $dimension[0];
		$height = $dimension[1];

		$variables['orientation'] = 'landscape';
		if($width < $height){
			$variables['orientation'] = 'portrait';
		}

		$bc = '';
		$anchor = '';
		$next_id = -1;
		$play_icon = '';
		$previous_assigned = false;
		$previous_post_id = -1;
		foreach($chapter->lessons()->orderBy('chapter_lesson.position')->get() as $k=>$v){
			if($k%2 != 0){
				$bc = 'background-color:rgb(29, 43, 65, 0.9);';
			}else{
				$bc = '';
			}
			if($v->post_id==$lesson->post_id){
				if($next_id == -1){
					$next_id = $k+1;
				}
				$anchor = 'anchor';
				$play_icon = '<i class="fas fa-play-circle"></i>';
				if($previous_post_id != -1 && $previous_assigned == false){
					$variables['previous'] = '<a href="'.route('course.show',[$course->post_id,$chapter->post_id,$previous_post_id]).'">ก่อนหน้า</a>';
					$previous_assigned = true;
				}
			}
			if($next_id == $k){
				$variables['next'] = '<a href="'.route('course.show',[$course->post_id,$chapter->post_id,$v->post_id]).'">ต่อไป</a>';
			}
			$k++;
			$variables['chapter_menu'] .= '<a class="py-1 px-2 rounded text-nowrap d-flex justify-content-between mb-0"   
				id="'.$anchor.'" 
				href="'.route('course.show',[$course->post_id,$chapter->post_id,$v->post_id]).'" 
				style="'.$bc.'">
					<div class="d-inline-block text-center">
						'.$play_icon.' 
						'.$v->title.'
					</div>
				<div>'.$v->duration_show.'</div>
			</a>';
			$anchor = '';
			$previous_post_id = $v->post_id;
			$play_icon = '';
		}

        return view('course::chapter.show',$variables);
    }

    public function formatMilliseconds($milliseconds) {
	    $seconds = floor($milliseconds / 1000);
	    $minutes = floor($seconds / 60);
	    $hours = floor($minutes / 60);
	    $milliseconds = $milliseconds % 1000;
	    $seconds = $seconds % 60;
	    $minutes = $minutes % 60;

	 	// $format = '%u:%02u:%02u.%03u';
		// $time = sprintf($format, $hours, $minutes, $seconds, $milliseconds);
		$format = '%u ช.ม. %02u นาที';
		$time = sprintf($format, number_format($hours), $minutes);
	    if(empty($hours)){
			$format = '%02u:%02u';
			$time = sprintf($format, $minutes, $seconds);
		}
	    // return rtrim($time, '0');
	    return $time;
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FirstName  $firstName
     * @return \Illuminate\Http\Response
     */
    public function edit(FirstName $firstName)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FirstName  $firstName
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FirstName $firstName)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FirstName  $firstName
     * @return \Illuminate\Http\Response
     */
    public function destroy(FirstName $firstName)
    {
        //
    }
}
