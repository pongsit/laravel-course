<?php

use Illuminate\Support\Facades\Route;
use Pongsit\Role\Http\Controllers\RoleController;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


// Route::get('/role', [RoleController::class, 'index'])->name('role')->middleware('auth');
// Route::get('/role/show/avatar/{name}/{size}', [RoleController::class, 'showAvatar'])->name('role.showAvatar');
// Route::post('/role/update/avatar/{id}', [RoleController::class, 'updateAvatar']);

// Route::get('/admin/dashboard', [RoleController::class, 'adminDashboard'])->name('admin.dashboard')->middleware('isAdmin');