<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id')->unique();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('duration')->nullable();
            $table->string('duration_show')->nullable();
            $table->json('lists')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('chapters', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id')->unique();
            $table->string('title')->nullable();
            $table->text('thumb')->nullable();
            $table->text('description')->nullable();
            $table->string('visibility')->default('paid');
            $table->string('duration')->nullable();
            $table->string('duration_show')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->text('doc')->nullable();
            $table->text('doc2')->nullable();
            $table->text('doc_sample')->nullable();
            $table->json('lists')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('lessons', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id')->unique();
            $table->string('title')->nullable();
            $table->text('thumb')->nullable();
            $table->text('description')->nullable();
            $table->string('visibility')->default('inherit');
            $table->text('shortcode')->nullable();
            $table->text('video')->nullable();
            $table->text('duration')->nullable();
            $table->string('duration_show')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('chapter_course', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('chapter_id');
            $table->bigInteger('course_id');
            $table->smallInteger('position');
            $table->timestamps();
        });

        Schema::create('chapter_lesson', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('chapter_id');
            $table->bigInteger('lesson_id');
            $table->smallInteger('position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
        Schema::dropIfExists('chapters');
        Schema::dropIfExists('lessons');
        Schema::dropIfExists('chapter_course');
        Schema::dropIfExists('course_lesson');
    }
}
